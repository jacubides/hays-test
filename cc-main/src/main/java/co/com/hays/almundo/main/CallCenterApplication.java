package co.com.hays.almundo.main;

import co.com.hays.almundo.enums.EmployeeTypeEnum;
import co.com.hays.almundo.impl.RequestResolver;
import co.com.hays.almundo.main.config.CCConfig;
import co.com.hays.almundo.model.Employee;
import co.com.hays.almundo.model.EmployeeType;
import co.com.hays.almundo.repositories.EmployeeRepository;
import co.com.hays.almundo.repositories.EmployeeTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import java.util.Scanner;

@SpringBootApplication
@Import(CCConfig.class)
public class CallCenterApplication implements CommandLineRunner {

    @Autowired
    private RequestResolver requestResolver;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private EmployeeTypeRepository employeeTypeRepository;

    private Integer numOperarios;
    private Integer numSupervisores;
    private Integer numDirectores;
    private Integer numRequests;


    public static void main(String[] args) {
        SpringApplication.run(CallCenterApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        initialize();
        askParameters();
        executeParameters();

        for (int i = 0; i < numRequests; i++) {
            requestResolver.getRequest();
        }

    }

    /**
     * Metodo encargado de insertar el numero de empleados solicitados por la consola.
     */
    private void executeParameters() {
        for (int i = 1; i <= numOperarios; i++) {
            Employee emp = new Employee();
            emp.setName("Operador " + i);
            emp.setActive(true);
            emp.setEmployeeType(employeeTypeRepository.findByName(EmployeeTypeEnum.OPERADOR.name()));
            employeeRepository.save(emp);
        }

        for (int i = 1; i <= numSupervisores; i++) {
            Employee emp = new Employee();
            emp.setName("Supervisor " + i);
            emp.setActive(true);
            emp.setEmployeeType(employeeTypeRepository.findByName(EmployeeTypeEnum.SUPERVISOR.name()));
            employeeRepository.save(emp);
        }

        for (int i = 1; i <= numDirectores; i++) {
            Employee emp = new Employee();
            emp.setName("Director " + i);
            emp.setActive(true);
            emp.setEmployeeType(employeeTypeRepository.findByName(EmployeeTypeEnum.DIRECTOR.name()));
            employeeRepository.save(emp);
        }
    }

    /**
     * Metodo enacrgado de obtener los valores iniciales del numero de empleados.
     */
    private void askParameters() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Por favor ingrese el número de Operarios: ");
        numOperarios = scan.nextInt();

        System.out.print("Por favor ingrese el número de Supervisores: ");
        numSupervisores = scan.nextInt();

        System.out.print("Por favor ingrese el número de Directores: ");
        numDirectores = scan.nextInt();

        System.out.print("Por favor ingrese el número de Peticiones: ");
        numRequests = scan.nextInt();

    }

    /**
     * Metodo encargado de inicializar los valores de BD.
     */
    private void initialize() {
        EmployeeType type = new EmployeeType();
        type.setName(EmployeeTypeEnum.OPERADOR.name());
        type.setActive(true);
        employeeTypeRepository.save(type);

        type = new EmployeeType();
        type.setName(EmployeeTypeEnum.SUPERVISOR.name());
        type.setActive(true);
        employeeTypeRepository.save(type);

        type = new EmployeeType();
        type.setName(EmployeeTypeEnum.DIRECTOR.name());
        type.setActive(true);
        employeeTypeRepository.save(type);
    }
}
