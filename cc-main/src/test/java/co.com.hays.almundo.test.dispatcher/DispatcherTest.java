package java.co.com.hays.almundo.test.dispatcher;

import co.com.hays.almundo.enums.EmployeeTypeEnum;
import co.com.hays.almundo.impl.Dispatcher;
import co.com.hays.almundo.main.config.CCConfig;
import co.com.hays.almundo.model.Employee;
import co.com.hays.almundo.repositories.EmployeeRepository;
import co.com.hays.almundo.repositories.EmployeeTypeRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

//@RunWith(SpringRunner.class)
//@SpringBootTest
//public class DispatcherTest {
//
//    @Autowired
//    private Dispatcher dispatcher;
//    @Autowired
//    private EmployeeRepository employeeRepository;
//    @Autowired
//    private EmployeeTypeRepository employeeTypeRepository;
//
//    @Test
//    public void selectEmployee() {
//        Employee emp = new Employee();
//        emp.setName("Operador 1");
//        emp.setActive(true);
//        emp.setEmployeeType(employeeTypeRepository.findByName(EmployeeTypeEnum.OPERADOR.name()));
//        emp = employeeRepository.save(emp);
//
//        dispatcher.selectEmployee();
//    }
//}
