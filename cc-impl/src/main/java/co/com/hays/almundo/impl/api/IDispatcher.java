package co.com.hays.almundo.impl.api;

import co.com.hays.almundo.model.Employee;
import org.springframework.stereotype.Service;

@Service
public interface IDispatcher {

    void dispatcherCall(Employee employee) throws InterruptedException;

}
