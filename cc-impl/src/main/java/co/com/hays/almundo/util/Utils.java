package co.com.hays.almundo.util;

import java.util.Random;

public class Utils {

    private static final int MILS = 1000;

    static Random rand = new Random();

    public static long getRandomNumber(int range) {
        return rand.nextInt(range) * MILS;
    }
}
