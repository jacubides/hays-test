package co.com.hays.almundo.impl;

import co.com.hays.almundo.impl.api.IDispatcher;
import co.com.hays.almundo.model.Employee;
import co.com.hays.almundo.repositories.EmployeeRepository;
import co.com.hays.almundo.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class Dispatcher implements IDispatcher {

    @Autowired
    private EmployeeRepository employeeRepository;

    /**
     * Metodo encargado de ejecutar todos los hilos de ejecución y liberar al empleado seleccionado.
     *
     * @param employee objeto empleado con los datos del mismo.
     * @throws InterruptedException lanzada de ser interumpido el hilo de ejecucion.
     */
    @Async
    @Override
    public void dispatcherCall(Employee employee) throws InterruptedException {
        System.out.println(employee.getName() + " está atendiendo la petición ");
        Thread.sleep(Utils.getRandomNumber(10));
        releaseEmployee(employee);
    }

    /**
     * Metodo que actualiza el estado del empleado actual.
     *
     * @param emp Objeto empleado con los datos del mismo.
     */
    private void releaseEmployee(Employee emp) {
        emp.setActive(true);
        employeeRepository.save(emp);
    }
}
