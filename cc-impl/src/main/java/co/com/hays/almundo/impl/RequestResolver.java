package co.com.hays.almundo.impl;

import co.com.hays.almundo.enums.EmployeeTypeEnum;
import co.com.hays.almundo.impl.api.IDispatcher;
import co.com.hays.almundo.model.Employee;
import co.com.hays.almundo.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RequestResolver {

    @Autowired
    private IDispatcher dispatcher;
    @Autowired
    private EmployeeRepository employeeRepository;

    /**
     * Metodo encargado de buscar los empleados disponibles y asignarles la una peticion.
     *
     * @throws InterruptedException
     */
    public void getRequest() throws InterruptedException {
        Employee emp = null;
        Employee operador;
        Employee supervisor;
        Employee director;
        List<Employee> employeeList = employeeRepository.findActiveEmployees();

        operador = employeeList.stream()
                .filter(employee -> employee.getEmployeeType().getName().equals(EmployeeTypeEnum.OPERADOR.name()))
                .findFirst().orElse(null);

        supervisor = employeeList.stream()
                .filter(employee -> employee.getEmployeeType().getName().equals(EmployeeTypeEnum.SUPERVISOR.name()))
                .findFirst().orElse(null);

        director = employeeList.stream()
                .filter(employee -> employee.getEmployeeType().getName().equals(EmployeeTypeEnum.DIRECTOR.name()))
                .findFirst().orElse(null);

        if (operador != null) {
            operador.setActive(false);
            emp = employeeRepository.save(operador);
        } else if (supervisor != null) {
            supervisor.setActive(false);
            emp = employeeRepository.save(supervisor);
        } else if (director != null) {
            director.setActive(false);
            emp = employeeRepository.save(director);
        }
        if (emp != null) {
            dispatcher.dispatcherCall(emp);
        } else {
            System.out.println("En este momento no hay empleados disponibles");
        }

    }
}
