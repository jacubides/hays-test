package co.com.hays.almundo.repositories;

import co.com.hays.almundo.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    @Query("FROM Employee emp WHERE emp.active = true")
    List<Employee> findActiveEmployees();
}
