package co.com.hays.almundo.repositories;

import co.com.hays.almundo.model.EmployeeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeTypeRepository extends JpaRepository<EmployeeType, Long> {

    EmployeeType findByName(String name);
}
